import {_, nanoajax} from './utils';
import {extPhp} from './config';
import {getStoredSettings} from './data/index';

const docElem = document.documentElement;

const trackData = {
    screen:
        docElem.clientWidth + 'x' +
        docElem.clientHeight,
    uri: location.pathname + location.search + location.hash,
    referrer: document.referrer,
    random: Math.floor(Math.random() * 1e9),
    // Action and state set later.
};
const trackedActions = {
    // Pre-filled with actions to ignore.
    'show-gen': true,
};

export async function track(action) {
    if (trackedActions[action]) {
        return;
    }

    trackedActions[action] = true;

    const settings = await getStoredSettings();

    trackData.action = action;
    trackData.state = JSON.stringify({
        langs: settings.langs,
        cats: settings.cats,
        numSeenWords: settings.seenWords.length,
    });

    nanoajax.ajax({
        url: 'track' + extPhp,
        body: JSON.stringify(trackData),
        headers: {
            'Content-Type': 'application/json',
        },
    }, _.noop);
}

const humanEvents = ['mousemove', 'scroll', 'click', 'keydown'];

humanEvents.forEach(evt => docElem.addEventListener(evt, trackIsHuman));

function trackIsHuman(e) {
    humanEvents.forEach(evt => docElem.removeEventListener(evt, trackIsHuman));
    trackData.event = e.type;
    track('human');
    setTimeout(function () {
        track('interested');
    }, 10e3);
}
