let global;

if (process.env.BABEL_ENV !== 'node') {
    global = window;
} else {
    global = {};
    global.React = require('react');

    global.React.addons = {
        CSSTransitionGroup: require('react-addons-css-transition-group'),
    };

    global.ReactDOM = require('react-dom');
    global.Redux = require('redux');
    global.ReactRedux = require('react-redux');

    global.nanoajax = require('nanoajax');
    global._ = require('underscore');
}

export const React = global.React;
export const ReactDOM = global.ReactDOM;
export const Redux = global.Redux;
export const ReactRedux = global.ReactRedux;

export const nanoajax = global.nanoajax;
export const _ = global._;

export function autobindMethods(obj) {
    const keys = Object.getOwnPropertyNames(Object.getPrototypeOf(obj));
    keys.forEach(key => {
        if (
            // render() method doesn't need binding.
            key !== 'render'
            && !obj.hasOwnProperty(key)
            && typeof obj[key] === 'function'
        ) {
            obj[key] = obj[key].bind(obj);
        }
    });
}

export function classes(obj) {
    return _.pairs(obj)
        .filter(x => x[1])
        .map(x => x[0])
        .join(' ');
}


// Redux middleware
export function thunkMiddleware(store) {
    return next => action =>
        typeof action === 'function' ?
            action(store.dispatch, store.getState) :
            next(action);
}

export function vanillaPromiseMiddleware(store) {
    return next => action => {
        if (typeof action.then !== 'function') {
            return next(action);
        }

        return Promise.resolve(action).then(store.dispatch);
    };
}

// URLs
export function basename(url) {
    return url.replace(/^.*\//, '');
}
