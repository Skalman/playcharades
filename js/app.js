import {translate} from './data/index';
import {basename} from './utils';

export function langChange(lang) {
    if (process.env.BABEL_ENV !== 'node') {
        document.title = translate(lang, 'app.name');
        document.documentElement.lang = lang;

        const newUrl = translate(lang, 'url.home');
        if (basename(newUrl) !== basename(location.pathname)) {
            history.pushState(null, translate(lang, 'app.name'), newUrl);
        }
    }
}
