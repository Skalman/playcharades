export const extHtml = process.env.NODE_ENV === 'development' ? '.html' : '';
export const extPhp = process.env.NODE_ENV === 'development' ? '.php' : '';

export const localStorageKey = 'charades';
export const localStorageTime = 7 * 24 * 60 * 60 * 1000;
