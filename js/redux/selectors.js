import {_} from '../utils';
import {translate, langs, cats} from '../data/index';

function selectPrint(numWords, words, langs) {
    // Options: 50, 100, or all words.
    const opts = [
        { number: 50 },
        { number: 100 },
        { number: words.length, all: true },
    ].filter(o => o.number < words.length || o.all);

    let numWordsSelected;
    if (numWords < words.length) {
        // Round up.
        numWordsSelected =
            opts.find(o => numWords <= o.number).number;
    } else {
        // Round down.
        numWordsSelected = words.length;
    }


    const selectedWords = _.shuffle(words).slice(0, numWordsSelected);

    return {
        numWordsSelected: numWordsSelected,
        numWordOptions: opts,
        selectedWords: selectedWords,
    };
}

function selectTranslate(mainLang) {
    return translate(mainLang);
}

export function select(state) {
    let generator = state.generator;
    if (!isValidWords(state.settings.langs, generator.words)) {
        generator = Object.assign({}, generator, {
            words: [],
            wordsLeft: [],
        });
    }
    return {
        translate: selectTranslate(state.settings.langs[0]),
        show: state.show,
        selectedLangs: state.settings.langs,
        selectedCats: state.settings.cats,
        langs: langs,
        cats: cats,
        generator: generator,
        print: selectPrint(
            state.print.numWordsSelected,
            generator.words,
            state.settings.langs),
    };
}

export function selectSettings(state) {
    return state.settings;
}

export function selectTracking(state) {
    return {
        show: state.show,
        langs: state.settings.langs.join(),
        cats: state.settings.cats.join(),
        currentWord: state.generator.currentWord
            && JSON.stringify(state.generator.currentWord),
    };
}

function isValidWords(langs, words) {
    return words.every(w => langs.every(l => w[l]));
}
