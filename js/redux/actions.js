import {_} from '../utils';
import * as actions from './actionsConstants';
import {getWords, viewWord, clearStoredSettings} from '../data/index';
import {langChange} from '../app';

const _receivedWords = make(actions.GEN_RECEIVED_WORDS, 'words', 'wordsLeft');
const _setLangs = make(actions.SET_LANGS, 'langs');
const _setCats = make(actions.SET_CATS, 'cats');
const _clearSettings = make(actions.CLEAR_SETTINGS);
const _print = make(actions.PRINT);
const _genSetNextWord = make(actions.GEN_SET_NEXT_WORD, 'word', 'wordsLeft');

// Simple action creators.
export const showHome = make(actions.SHOW_HOME);
export const showGen = make(actions.SHOW_GEN);
export const showPrint = make(actions.SHOW_PRINT);

export const printSelectNumWords =
    make(actions.PRINT_SELECT_NUM_WORDS, 'numWords');

export const noop = make(actions.NOOP);


// Makes a simple action creator.
function make(type, ...argNames) {
    return function (...args) {
        const action = { type };
        argNames.forEach((arg, index) => {
            action[argNames[index]] = args[index];
        });
        return action;
    };
}


export function updateWords() {
    return (dispatch, getState) => {
        return getWords(getState().settings)
            .then(({words, wordsLeft}) => {
                dispatch(_receivedWords(
                    _.shuffle(words),
                    _.shuffle(wordsLeft),
                ));

                const word = getState().generator.currentWord;
                if (words.includes(word)) {
                    return dispatch(noop());
                } else {
                    return dispatch(genNextWord());
                }
            });
    };
}

export function setLangs(langs, getWords = true) {
    if (!langs.length) {
        return noop();
    }

    return (dispatch, getState) => {
        const prevLangs = getState().settings.langs;
        if (prevLangs[0] !== langs[0]) {
            langChange(langs[0]);
        }

        dispatch(_setLangs(langs));

        if (getWords) {
            return dispatch(updateWords());
        }
    };
}

export function setCats(cats, getWords = true) {
    if (!cats.length) {
        return noop();
    }

    return (dispatch, getState) => {
        dispatch(_setCats(cats));

        if (getWords) {
            return dispatch(updateWords());
        }
    };
}

export function clearSettings() {
    return (dispatch, getState) => {
        dispatch(_clearSettings());

        clearStoredSettings();

        return dispatch(updateWords());
    };
}

export function print() {
    window.print();
    return _print();
}

export function genNextWord() {
    return (dispatch, getState) => {
        const state = getState();

        const {words, wordsLeft} = state.generator;

        if (!state.generator.words.length) {
            // There aren't any words (yet).
            return dispatch(updateWords());
        }

        let newWordsLeft = wordsLeft;

        if (!wordsLeft.length) {
            // Re-shuffle the words.
            newWordsLeft = _.shuffle(words);
        }

        const word = newWordsLeft[0];
        newWordsLeft = newWordsLeft.slice(1);

        viewWord({
            langs: state.settings.langs,
            word: word,
        });

        return dispatch(_genSetNextWord(word, newWordsLeft));
    };
}
