// Screens.
export const SHOW_HOME = 'SHOW_HOME';
export const SHOW_GEN = 'SHOW_GEN';
export const SHOW_PRINT = 'SHOW_PRINT';

// Word generator.
export const GEN_SET_NEXT_WORD = 'SET_GEN_NEXT_WORD';
export const GEN_RECEIVED_WORDS = 'GEN_RECEIVED_WORDS';

// Settings.
export const SET_LANGS = 'SET_LANGS';
export const SET_CATS = 'SET_CATS';
export const CLEAR_SETTINGS = 'CLEAR_SETTINGS';

// Print
export const PRINT = 'PRINT';
export const PRINT_SELECT_NUM_WORDS = 'PRINT_SELECT_NUM_WORDS';

// Utilities.
export const NOOP = 'NOOP';
