import * as actions from './actionsConstants';
import {Redux} from '../utils';
import {langs, cats} from '../data/index';

const initialSettings = {
    langs: [langs[0]],
    cats: [cats[0]],
};

const initialGenerator = {
    currentWord: null,
    words: [],
    wordsLeft: [],
};


const app = Redux.combineReducers({
    show,
    settings,
    generator,
    print,
});

export default app;

function show(state = 'home', action) {
    switch (action.type) {
        case actions.SHOW_HOME:
            return 'home';

        case actions.SHOW_GEN:
            return 'gen';

        case actions.SHOW_PRINT:
            return 'print';

        default:
            return state;
    }
}

function settings(state = initialSettings, action) {
    switch (action.type) {
        case actions.SET_LANGS:
            return Object.assign({}, state, { langs: action.langs });

        case actions.SET_CATS:
            return Object.assign({}, state, { cats: action.cats });

        case actions.CLEAR_SETTINGS:
            return initialSettings;

        default:
            return state;
    }
}


function generator(state = initialGenerator, action) {
    switch (action.type) {
        case actions.GEN_RECEIVED_WORDS:
            return Object.assign({}, state, {
                words: action.words,
                wordsLeft: action.wordsLeft,
            });

        case actions.GEN_SET_NEXT_WORD:
            return Object.assign({}, state, {
                currentWord: action.word,
                wordsLeft: action.wordsLeft,
            });

        default:
            return state;
    }
}

function print(state = { numWordsSelected: 50 }, action) {
    switch (action.type) {
        case actions.PRINT_SELECT_NUM_WORDS:
            return { numWordsSelected: action.numWords };

        case actions.PRINT:
        default:
            return state;
    }
}
