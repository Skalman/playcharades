import {
    React, Redux, ReactRedux, thunkMiddleware, vanillaPromiseMiddleware,
} from '../utils';
import appReducer from './reducers';
import {setLangs, setCats} from './actions';
import {getStoredSettings, setStoredSettings} from '../data/index';
import {selectSettings} from './selectors';
import {App} from '../containers/App';

const createStoreWithMiddleware = Redux.applyMiddleware(
    thunkMiddleware,
    vanillaPromiseMiddleware
)(Redux.createStore);

export function createStore() {
    const store = createStoreWithMiddleware(appReducer);
    return store;
}

export function createStoreWithStoredState() {
    return getStoredSettings()
    .then(settings => {
        const store = createStore();
        store.dispatch(setLangs(settings.langs, false));
        store.dispatch(setCats(settings.cats, false));

        let last;
        store.subscribe(() => {
            const newSettings = selectSettings(store.getState());
            if (last !== newSettings) {
                setStoredSettings(newSettings);
                last = newSettings;
            }
        });

        return store;
    });
}

export function createProvider(store) {
    return (<ReactRedux.Provider store={store} >
        <App />
    </ReactRedux.Provider>);
}
