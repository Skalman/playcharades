import {React, ReactRedux, autobindMethods} from '../utils';
import {select} from '../redux/selectors';
import * as actions from '../redux/actions';
import {Print} from '../components/Print';
import {Header} from '../components/Header';
import {Footer} from '../components/Footer';
import {Generator} from '../components/Generator';
import {Settings} from '../components/Settings';
import {List} from '../components/List';

const p = React.PropTypes;

const arrayOfString = p.arrayOf(
    p.string.isRequired
).isRequired;

const pWord = p.objectOf(p.string.isRequired);

class AppComponent extends React.Component {
    static propTypes = {
        dispatch: p.func.isRequired,
        translate: p.func.isRequired,
        show: p.string.isRequired,
        selectedLangs: arrayOfString,
        selectedCats: arrayOfString,
        langs: arrayOfString,
        cats: arrayOfString,
        print: p.shape({
            selectedWords: p.arrayOf(pWord.isRequired).isRequired,
            numWordsSelected: p.number.isRequired,
            numWordOptions: p.arrayOf(
                p.shape({
                    number: p.number.isRequired,
                    all: p.bool,
                }).isRequired
            ).isRequired,
        }).isRequired,
        generator: p.shape({
            currentWord: pWord,
            words: p.arrayOf(pWord.isRequired).isRequired,
            wordsLeft: p.arrayOf(pWord.isRequired).isRequired,
        }).isRequired,
    };

    constructor(props) {
        super(props);
        autobindMethods(this);
    }

    handleGenNextWord() {
        this.props.dispatch(actions.genNextWord());
    }

    handleGenNextWordAndShowGen() {
        this.props.dispatch(actions.genNextWord());
        this.props.dispatch(actions.showGen());
    }

    handleQuit() {
        this.props.dispatch(actions.showHome());
    }

    handleSelectLangs(langs) {
        this.props.dispatch(actions.setLangs(langs));
    }

    handleSelectCats(cats) {
        this.props.dispatch(actions.setCats(cats));
    }

    handleShowPrint(e) {
        e.preventDefault();
        this.props.dispatch(actions.showPrint());
    }

    handleClearSettings(e) {
        e.preventDefault();
        this.props.dispatch(actions.clearSettings());
    }

    handlePrintSelectNumWords(numWords) {
        this.props.dispatch(actions.printSelectNumWords(numWords));
    }

    handlePrint() {
        this.props.dispatch(actions.print());
    }

    renderHomeOrGen(show) {
        const {
            translate,
            selectedLangs, selectedCats, langs, cats,
            generator,
        } = this.props;

        return (<div className="charades-app">
            <Header
                translate={translate}
                includeTagLine={show === 'home'}
            />
            <Generator
                translate={translate}
                simple={show === 'home'}
                langs={selectedLangs}
                currentWord={generator.currentWord}
                words={generator.words}
                wordsLeft={generator.wordsLeft}
                onGenNextWord={this.handleGenNextWordAndShowGen}
                onQuit={this.handleQuit}
            />
            <React.addons.CSSTransitionGroup
                component="div"
                transitionName="fade"
                transitionEnterTimeout={300}
                transitionLeaveTimeout={300}
            >{show === 'home' && <div key={0}>
                <Settings
                    translate={translate}
                    langs={langs}
                    cats={cats}
                    selectedLangs={selectedLangs}
                    selectedCats={selectedCats}
                    onSelectLangs={this.handleSelectLangs}
                    onSelectCats={this.handleSelectCats}
                    className="space-below"
                />
                <section>
                    <List className="list-inline">
                        <a href={translate('url.howToPlay')}>
                            {translate('btn.howToPlay')}
                        </a>
                        <a href={translate('url.suggest')}>
                            {translate('btn.suggestWords')}
                        </a>
                        <a
                            href=""
                            onClick={this.handleShowPrint}
                        >
                            {translate('btn.printVersion')}
                        </a>
                        <a
                            href=""
                            onClick={this.handleClearSettings}
                        >
                            {translate('btn.clearSettings')}
                        </a>
                    </List>
                </section>
                <Footer translate={translate} />
            </div>}</React.addons.CSSTransitionGroup>
        </div>);
    }

    renderPrint() {
        const { translate, selectedLangs, print } = this.props;

        return (
            <Print
                {...print}
                langs={selectedLangs}
                translate={translate}
                onSelectNumWords={this.handlePrintSelectNumWords}
                onQuit={this.handleQuit}
                onPrint={this.handlePrint}
            />
        );
    }

    render() {
        const show = this.props.show;

        if (show === 'home' || show === 'gen') {
            return this.renderHomeOrGen(show);
        } else if (show === 'print') {
            return this.renderPrint();
        } else {
            return <div>{`Unknown screen ${show}`}</div>;
        }
    }
}

// Wrap the component to inject dispatch and state into it
export const App = ReactRedux.connect(select)(AppComponent);
