import {_} from '../utils';
import {texts} from './translate';
import {words} from './words';
import {localStorageKey, localStorageTime} from '../config';

export const langs = ['en', 'sv'];

export const cats = [
    'easy',
    'moderate',
    'hard',
    'veryHard',
    'phrases',
    'celebrities',
];

export async function getWords({langs, cats}) {
    if (!_settings) {
        getStoredSettingsSync();
    }

    // Get words that match chosen categories.
    let matchedWords = _.flatten(cats.map(cat => words[cat]));

    // Filter out words that don't exist in all chosen languages.
    matchedWords = matchedWords.filter(word => langs.every(l => word[l]));

    const wordsLeft =
        matchedWords.filter(word => !hasSeenWord({ langs, word }));

    return {
        words: matchedWords,
        wordsLeft: wordsLeft,
    };
}

let _settings; // Initialized later.

function _hasSeenWord(lang, word) {
    return _settings.seenWords.includes(`${lang} ${word}`);
}

export function hasSeenWord({langs, word}) {
    if (!_settings) {
        getStoredSettingsSync();
    }

    return langs.some(lang => _hasSeenWord(lang, word[lang]));
}

function _viewWord(lang, word) {
    if (!_hasSeenWord(lang, word)) {
        _settings.seenWords.push(`${lang} ${word}`);
    }
}

export function viewWord({langs, word}) {
    if (!_settings) {
        getStoredSettingsSync();
    }

    const lengthBefore = _settings.seenWords.length;

    langs.forEach(lang => _viewWord(lang, word[lang]));

    if (_settings.seenWords.length !== lengthBefore) {
        setStoredSettings();
    }
}

function getStoredSettingsSync() {
    let json;
    try {
        json = localStorage.getItem(localStorageKey);
    } catch (e) {
        warnLocalStorageError();
    }

    const obj = json && JSON.parse(json);

    if (obj && Date.now() < obj.lastUpdate + localStorageTime) {
        // Not too old.
        _settings = obj;
    } else {
        _settings = {
            lastUpdate: 0,
            seenWords: [],
            langs: ['en'],
            cats: ['easy'],
        };
    }

    return _settings;
}

export async function getStoredSettings() {
    return getStoredSettingsSync();
}

export async function setStoredSettings(newSettings) {
    newSettings = Object.assign(
        {},
        newSettings || _settings,
        {
            lastUpdate: Date.now(),
            seenWords: _settings.seenWords,
        }
    );
    _settings = newSettings;

    try {
        localStorage.setItem(
            localStorageKey,
            JSON.stringify(newSettings)
        );
    } catch (e) {
        warnLocalStorageError();
    }
}

export async function clearStoredSettings() {
    _settings = undefined;
    try {
        localStorage.removeItem(localStorageKey);
    } catch (e) {
        warnLocalStorageError();
    }
}

/* eslint-disable no-console */
let warnLocalStorageError = _.once(() =>
    console.error(
        'SettingsStore',
        'Cannot use localStorage to store settings'
    )
);
/* eslint-enable no-console */
if (process.env.BABEL_ENV === 'node') {
    warnLocalStorageError = _.noop;
}


export function getLangName(lang) {
    return texts.autonym[lang] || lang;
}

export function translate(lang, textId, fallback) {
    if (textId === undefined) {
        const translator = _.partial(translate, lang);
        translator.lang = lang;
        return translator;
    }

    if (textId.indexOf('_') !== -1) {
        return textId;
    }

    textId = textId.replace(/\./g, '_');
    const text = texts[textId];

    return text && text[lang] || (fallback !== undefined ? fallback : textId);
}

export function getTranslations(textId) {
    textId = textId.replace(/\./g, '_');
    return texts[textId];
}
