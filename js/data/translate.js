import {extHtml, extPhp} from '../config';

const copyrightPart = '<a href="http://danwolff.se">Dan Wolff</a> (2016)';

export const texts = {
    autonym: {en: 'English', sv: 'Svenska'},

    app_name: {en: 'Play Charades', sv: 'Spela charader'},
    app_tagline: {en: '— 300+ random words', sv: '– 300+ slumpade ord'},

    btn_startGame: {en: 'Start', sv: 'Börja'},
    btn_nextWord: {en: 'Next', sv: 'Nästa'},
    btn_back: {en: 'Back', sv: 'Tillbaka'},
    btn_clearSettings: {en: 'Clear settings', sv: 'Rensa inställningar'},
    btn_printVersion: {en: 'Print version', sv: 'Utskriftsversion'},
    btn_print: {en: 'Print', sv: 'Skriv ut'},
    btn_howToPlay: {en: 'How to play charades', sv: 'Hur man spelar charader'},
    btn_suggestWords: {en: 'Word suggestions?', sv: 'Förslag på ord?'},

    cat_easy: {en: 'Easy', sv: 'Lätt'},
    cat_moderate: {en: 'Moderate', sv: 'Mellan'},
    cat_hard: {en: 'Hard', sv: 'Svår'},
    cat_veryHard: {en: 'Very hard', sv: 'Mycket svår'},
    cat_phrases: {en: 'Phrases', sv: 'Fraser'},
    cat_celebrities: {en: 'Celebrities', sv: 'Kändisar'},
    cat_characters: {en: 'Characters', sv: 'Rollfigurer'},
    cat_moviesAndTvShows: {
        en: 'Movies and TV shows',
        sv: 'Filmer och tv-serier',
    },
    cat_songs: {en: 'Songs', sv: 'Sånger'},
    cat_books: {en: 'Books', sv: 'Böcker'},

    opts_languages: {en: 'Languages', sv: 'Språk'},
    opts_multipleLanguages: {en: 'Multiple languages', sv: 'Flera språk'},
    opts_categories: {en: 'Categories', sv: 'Kategorier'},

    opts_printXWords: {en: '{0} words', sv: '{0} ord'},
    opts_printAllXWords: {en: 'All {0} words', sv: 'Alla {0} ord'},

    text_copyrightHtml: {
        en: `Play Charades by ${copyrightPart}`,
        sv: `Spela charader, av ${copyrightPart}`,
    },
    text_spread: {
        en: 'If you enjoy this site, spread the word',
        sv: 'Om du gillar sajten, berätta gärna för andra',
    },
    text_printTagline: {
        en: '— print and cut out',
        sv: '– skriv ut och klipp ut',
    },

    url_home: {
        en: './',
        sv: 'spela-charader' + extHtml,
    },
    url_howToPlay: {
        en: 'how-to-play' + extHtml,
        sv: 'hur-man-spelar' + extHtml,
    },
    url_suggest: {
        en: 'suggest' + extPhp,
        sv: 'förslag-på-ord' + extPhp,
    },
};
