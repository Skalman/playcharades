// import './development';
//
import { ReactDOM, basename } from './utils';
import {createStoreWithStoredState, createProvider} from './redux/store';
import {genNextWord, setLangs} from './redux/actions';
import {selectTracking} from './redux/selectors';
import {getTranslations} from './data/index';
import {track} from './track';

if (document.readyState === 'complete') {
    init();
} else {
    document.addEventListener('DOMContentLoaded', init);
}

function init() {
    createStoreWithStoredState()
    .then(store => {
        ReactDOM.render(
            createProvider(store),
            document.getElementById('app')
        );
        store.dispatch(genNextWord());

        addEventListener('popstate', e => {
            const url = basename(location.pathname);
            const urls = getTranslations('url.home');

            for (const lang in urls) {
                if (url === basename(urls[lang])) {
                    store.dispatch(setLangs([lang]));
                    return;
                }
            }
        });

        let last = selectTracking(store.getState());
        store.subscribe(() => {
            const cur = selectTracking(store.getState());

            if (last.show !== cur.show) {
                track(`show-${cur.show}`);
            } else if (last.langs !== cur.langs) {
                track('change-languages');
            } else if (last.cats !== cur.cats) {
                track('change-categories');
            } else if (last.currentWord !== cur.currentWord && last.currentWord && cur.currentWord && last.currentWord) {
                track('change-seenWords');
            }

            last = cur;
        });

    });
}
