if (process.env.NODE_ENV !== 'production') {
    // Simplify development by reloading the webpage when the page is refocused.
    // Poor man's hot reload.

    let buildDate;

    // Initialize build date.
    getBuildDate(date => {
        buildDate = date;
    });

    window.onblur = function () {
        window.onfocus = () => reloadIfNewer(buildDate);
    };
}

function reloadIfNewer(prevBuildDate) {
    getBuildDate(date => {
        if (date !== prevBuildDate) {
            /* eslint-disable no-console */
            console.log(`Reload scripts and styles (new build date: ${date})`);
            /* eslint-enable no-console */
            reloadScriptsAndStyles();
        }
    });
}

function getBuildDate(callback) {
    window.nanoajax.ajax(
        { url: `build-date.txt?${Date.now() }` },
        (code, responseText) => {
            callback(responseText);
        });
}

function reloadScriptsAndStyles() {
    window.onfocus = null;
    document.getElementById('app').innerHTML = '';

    const elems = Array.prototype.slice.call(
        document.querySelectorAll('script, link[rel=stylesheet]')
    );
    elems.forEach(oldElem => {
        if (oldElem.isReplacement) {
            oldElem.remove();
            return;
        }
        let elem;
        const newUrl = (
            (oldElem.src || oldElem.href).replace(/\?.*/, '')
            + '?'
            + Date.now()
        );

        if (oldElem.src) {
            elem = document.createElement('script');
            elem.src = newUrl;
        } else {
            elem = document.createElement('link');
            elem.rel = 'stylesheet';
            elem.href = newUrl;
        }
        elem.isReplacement = true;
        oldElem.parentNode.insertBefore(elem, oldElem);
    });

}
