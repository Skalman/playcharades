import {React} from '../utils';

export class Word extends React.Component {
    static propTypes = {
        langs: React.PropTypes.arrayOf(
            React.PropTypes.string.isRequired
        ).isRequired,
        word: React.PropTypes.objectOf(
            React.PropTypes.string.isRequired
        ),
        animate: React.PropTypes.bool,
    };

    render() {
        let wordTranslations;

        if (!this.props.word) {
            wordTranslations = this.props.langs.map(l => '\xA0');
        } else {
            wordTranslations = this.props.langs.map(l => this.props.word[l]);
        }

        const items = wordTranslations.map((word, i) =>
            <li key={this.props.langs[i]}>{word}</li>);

        if (this.props.animate) {
            return (
                <React.addons.CSSTransitionGroup
                    component="ul"
                    className="list-unstyled fade-move"
                    transitionName="fade"
                    transitionEnterTimeout={300}
                    transitionLeaveTimeout={300}
                >
                    {items}
                </React.addons.CSSTransitionGroup>
            );
        } else {
            return <ul className="list-unstyled">{items}</ul>;
        }
    }
}
