import {React} from '../utils';

export class Header extends React.Component {
    static propTypes = {
        translate: React.PropTypes.func.isRequired,
        includeTagLine: React.PropTypes.bool.isRequired,
    };

    render() {
        return (<h1>
            <img
                src="img/stick-figure.png"
                alt=""
            />
            {' '}
            {this.props.translate('app.name')}
            {' '}
            <React.addons.CSSTransitionGroup
                className="fade-move"
                transitionName="fade"
                transitionEnterTimeout={300}
                transitionLeaveTimeout={300}
            >{this.props.includeTagLine &&
                <small key={0}>
                    {this.props.translate('app.tagline')}
                </small>
            }</React.addons.CSSTransitionGroup>
        </h1>);
    }
}
