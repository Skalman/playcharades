import {_, React, autobindMethods} from '../utils';
import {ListToggler} from './ListToggler';
import {getLangName, translate} from '../data/index';

const arrayOfString = React.PropTypes.arrayOf(
    React.PropTypes.string.isRequired
).isRequired;

/* eslint-disable react/no-set-state */
export class Settings extends React.Component {
    static propTypes = {
        selectedLangs: arrayOfString,
        selectedCats: arrayOfString,
        langs: arrayOfString,
        cats: arrayOfString,
        onSelectLangs: React.PropTypes.func.isRequired,
        onSelectCats: React.PropTypes.func.isRequired,
        translate: React.PropTypes.func.isRequired,
        className: React.PropTypes.string,
    };

    constructor(props) {
        super(props);
        autobindMethods(this);

        this.state = {
            multipleLangs: this.props.selectedLangs.length > 1,
        };
    }

    _toggle(list, item) {
        if (!list.includes(item)) {
            return list.concat([item]);
        } else if (list.length > 1) {
            return list.filter(i => i !== item);
        } else {
            return list;
        }
    }

    handleToggleLang(lang) {
        this.props.onSelectLangs(this._toggle(this.props.selectedLangs, lang));
    }

    handleToggleCat(cat) {
        this.props.onSelectCats(this._toggle(this.props.selectedCats, cat));
    }

    handleSelectLang(lang) {
        this.props.onSelectLangs([lang]);
    }

    selectCat(cat) {
        this.props.onSelectCats([cat]);
    }

    handleToggleMultipleLangs() {
        if (this.state.multipleLangs) {
            this.props.onSelectLangs([this.props.selectedLangs[0]]);
        } else {
            this.props.onSelectLangs(_.uniq(
                [this.props.selectedLangs[0]].concat(this.props.langs)
            ));
        }
        this.setState({
            multipleLangs: !this.state.multipleLangs,
        });
    }

    render() {
        const trans = this.props.translate;

        return (<div className={this.props.className}>
            <section>
                <h2>{trans('opts.languages')}</h2>
                <ListToggler
                    onToggle={
                        this.state.multipleLangs
                        ? this.handleToggleLang
                        : this.handleSelectLang
                    }
                    items={this.props.langs.map(l => ({
                        id: l,
                        text: getLangName(l),
                        selected: this.props.selectedLangs.includes(l),
                        url: translate(l, 'url.home'),
                    }))}
                    multiple={this.state.multipleLangs}
                    id="langs"
                />
                <p>
                    <label>
                        <input
                            type="checkbox"
                            onChange={this.handleToggleMultipleLangs}
                            checked={this.state.multipleLangs}
                        />
                        {' '}
                        {trans('opts.multipleLanguages')}
                    </label>
                </p>
            </section>

            <section>
                <h2>{trans('opts.categories')}</h2>
                <ListToggler
                    onToggle={this.handleToggleCat}
                    items={this.props.cats.map(c => ({
                        id: c,
                        text: trans(`cat.${c}`),
                        selected: this.props.selectedCats.includes(c),
                    }))}
                    multiple
                    id="cats"
                />
            </section>
        </div>);
    }
}
