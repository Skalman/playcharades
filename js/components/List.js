import {React} from '../utils';

export class List extends React.Component {
    static propTypes = {
        children: React.PropTypes.arrayOf(
            React.PropTypes.node.isRequired
        ).isRequired,
    };

    render() {
        const children = this.props.children
            .map(c =>
                Array.isArray(c)
                    ? c.map((gc, i) =>
                        <li key={gc.key == undefined ? i : gc.key}>{gc}</li>)
                    : <li>{c}</li>
            );

        const props = Object.assign({}, this.props, {children});

        return <ul {...props}></ul>;
    }
}
