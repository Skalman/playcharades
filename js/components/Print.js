import {_, React} from '../utils';
import {List} from './List';
import {ListToggler} from './ListToggler';
import {Word} from './Word';

const p = React.PropTypes;

const arrayOfString = p.arrayOf(
    p.string.isRequired
).isRequired;

const pWord = p.objectOf(p.string.isRequired);

export class Print extends React.Component {
    static propTypes = {
        translate: p.func.isRequired,
        langs: arrayOfString,
        selectedWords: p.arrayOf(pWord.isRequired).isRequired,
        numWordsSelected: p.number.isRequired,
        numWordOptions: p.arrayOf(
            p.shape({
                number: p.number.isRequired,
                all: p.bool,
            }).isRequired
        ).isRequired,
        onQuit: p.func.isRequired,
        onPrint: p.func.isRequired,
        onSelectNumWords: p.func.isRequired,
    };

    render() {
        const trans = this.props.translate;

        const opts = this.props.numWordOptions.map(o => ({
            id: o.number,
            text: trans(o.all ? 'opts.printAllXWords' : 'opts.printXWords')
                .replace('{0}', o.number),
            selected: o.number === this.props.numWordsSelected,
        }));

        // Sort words by length, the shortest first. This should make for
        // easier cut-outs.
        const words = _.sortBy(
            this.props.selectedWords,
            word => _.max(
                this.props.langs.map(lang => word[lang].length)
            )
        );


        return (<div>
            <section className="noprint container">
                <h1>
                    {trans('btn.printVersion')}
                    {' '}
                    <small>{trans('text.printTagline')}</small>
                </h1>
                <List className="list-inline">
                    <button
                        onClick={this.props.onQuit}
                        className="btn"
                    >
                        {trans('btn.back')}
                    </button>
                    <button
                        onClick={this.props.onPrint}
                        className="btn"
                    >
                        {trans('btn.print')}
                    </button>
                    <ListToggler
                        onToggle={this.props.onSelectNumWords}
                        items={opts}
                        id="printNumWords"
                    />
                </List>
            </section>
            <List
                className="list-unstyled cards"
                children={
                    words.map((w, i) =>
                        <Word
                            key={i}
                            langs={this.props.langs}
                            word={w}
                        />
                    )
                }
            />
        </div>);
    }
}
