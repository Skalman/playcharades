import {React, autobindMethods} from '../utils';

export class ListTogglerItem extends React.Component {
    static propTypes = {
        /* eslint-disable react/forbid-prop-types */
        id: React.PropTypes.any.isRequired,
        /* eslint-enable react/forbid-prop-types */
        listId: React.PropTypes.string.isRequired,
        index: React.PropTypes.number.isRequired,
        multiple: React.PropTypes.bool,
        selected: React.PropTypes.bool.isRequired,
        text: React.PropTypes.string.isRequired,
        onToggle: React.PropTypes.func.isRequired,
        url: React.PropTypes.string,
    };

    constructor(props) {
        super(props);
        autobindMethods(this);
    }

    handleToggle(e) {
        if (e.target !== this.input) {
            e.preventDefault();
        }
        this.props.onToggle(this.props.id);
        this.input.focus();
    }

    refInput(input) {
        this.input = input;
    }

    render() {
        const {listId, index} = this.props;
        let labelContent;
        if (this.props.url) {
            labelContent = (
                <a
                    href={this.props.url}
                    tabIndex={-1}
                    onClick={this.handleToggle}
                >
                    {this.props.text}
                </a>
            );
        } else {
            labelContent = this.props.text;
        }

        return (<li>
            <input
                ref={this.refInput}
                id={`${listId}-${index}`}
                name={listId}
                type={this.props.multiple ? 'checkbox' : 'radio'}
                onChange={this.handleToggle}
                checked={this.props.selected}
            />
            <label htmlFor={`${listId}-${index}`}>{labelContent}</label>
        </li>);
    }
}
