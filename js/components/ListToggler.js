import {React} from '../utils';
import {ListTogglerItem} from './ListTogglerItem';

export class ListToggler extends React.Component {
    static propTypes = {
        items: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                id: React.PropTypes.any.isRequired,
            }).isRequired
        ).isRequired,
        multiple: React.PropTypes.bool,
        onToggle: React.PropTypes.func.isRequired,
        id: React.PropTypes.string.isRequired,
    };

    render() {
        return (<ul className="list-inline list-toggle">
            {this.props.items.map((item, i) =>
                <ListTogglerItem
                    key={i}
                    id={item.id}
                    listId={this.props.id}
                    index={i}
                    multiple={this.props.multiple}
                    selected={item.selected}
                    onToggle={this.props.onToggle}
                    text={item.text}
                    url={item.url}
                />
            )}
        </ul>);
    }
}
