import {React} from '../utils';

/* eslint-disable react/no-danger */

export class Footer extends React.Component {
    static propTypes = {
        translate: React.PropTypes.func.isRequired,
        includeTagLine: React.PropTypes.bool,
    };

    render() {
        return (<footer>
            <ul className="list-inline">
                <li
                    dangerouslySetInnerHTML={
                        {__html: this.props.translate('text.copyrightHtml')}
                    }
                >
                </li>
                <li>
                    {this.props.translate('text.spread')}
                </li>
            </ul>
        </footer>);
    }
}
