import {React, autobindMethods} from '../utils';
import {Word} from './Word';

const p = React.PropTypes;

const arrayOfString = p.arrayOf(
    p.string.isRequired
).isRequired;

const pWord = p.objectOf(p.string.isRequired);

export class Generator extends React.Component {
    static propTypes = {
        translate: p.func.isRequired,
        currentWord: pWord,
        words: p.arrayOf(pWord.isRequired).isRequired,
        wordsLeft: p.arrayOf(pWord.isRequired).isRequired,
        onQuit: p.func.isRequired,
        onGenNextWord: p.func.isRequired,
        langs: arrayOfString,
        simple: p.bool,
    };

    constructor(props) {
        super(props);
        autobindMethods(this);
    }

    handleGetNextWord(e) {
        e.preventDefault();
        this.props.onGenNextWord();
    }

    getStatusString() {
        return (
            (this.props.words.length - this.props.wordsLeft.length)
            + '/'
            + this.props.words.length
        );
    }

    render() {
        const trans = this.props.translate;
        return (<div className="generator">
            <a
                href=""
                className="generated-words"
                onClick={this.handleGetNextWord}
            >
                <Word
                    langs={this.props.langs}
                    word={this.props.currentWord}
                    animate
                />
            </a>
            <React.addons.CSSTransitionGroup
                className="fade-move"
                transitionName="fade"
                transitionEnterTimeout={300}
                transitionLeaveTimeout={300}
            >{this.props.simple || <span className="inline-block">
                <button
                    className="btn"
                    onClick={this.props.onQuit}
                >
                    {trans('btn.back')}
                </button>
            </span>}</React.addons.CSSTransitionGroup>
            <button
                className="btn btn-main right"
                onClick={this.props.onGenNextWord}
            >
                {trans('btn.nextWord')}
            </button>
            <div
                className="status"
                hidden={this.props.simple}
            >
                {this.getStatusString()}
            </div>
        </div>);
    }
}
