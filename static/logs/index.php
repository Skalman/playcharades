<?php

	function parse_ua($ua) {
		static $oss = array(
			'#(Ubuntu|Fedora)/(\d+\.\d+)?#' => '$1 $2',
			'#Android( [0-9.]+)?#' => 'Android$1',
			'#(BSD|Ubuntu|Fedora|Debian|Linux)#' => '$1',

			'#iPhone OS (\d+)[_.](\d+)#' => 'iPhone $1.$2',
			'#iPad(; CPU)? OS (\d+)[_.](\d+)#' => 'iPad $2.$3',
			'#Mac OS X (\d+)[_.](\d+)#' => 'Mac $1.$2',
			
			'#Windows NT 6\.4#' => 'Win 10',
			'#Windows NT 6\.3#' => 'Win 8.1',
			'#Windows NT 6\.2#' => 'Win 8',
			'#Windows NT 6\.1#' => 'Win 7',
			'#Windows NT 6\.0#' => 'Win Vista',
			'#Windows NT 5\.1#' => 'Win XP',
			'#Windows NT 5\.0#' => 'Win 2000',
			'#Windows#' => 'Windows',
		);
		static $browsers = array(
			'#Opera (\d+\.\d+)#' => 'Opera $1',
			'#Opera.+?Version/(\d+\.\d+)#' => 'Opera $1',
			'#Opera/(\d+\.\d+)#' => 'Opera $1',

			'#MSIE (\d+\.\d+)#' => 'IE $1',
			'#Trident/.+?; rv:([0-9.]+)#' => 'IE $1',

			'#Firefox/(\d+\.\d+([ab]\d+)?)#' => 'Fx $1',
			'#rv:(\d+\.\d+([ab]\d+)?).+?Gecko/#' => 'Gecko $1',

			'#UCWEB#' => 'UC Browser',
			'#UCBrowser(/([0-9]+\.[0-9]+))?#' => 'UC Browser $2',

			'#Chrome/(\d+)#' => 'Chrome $1',

			'#Version/(\d+\.\d+).+?Safari#' => 'Safari $1',
			'#Safari/(\d+)#' => 'Safari $1',
		);
		$os = $browser = 'Unknown';

		$tmp;
		$str_replacer = array(null, '$1', '$2', '$3');
		foreach ($oss as $pattern => $replace) {
			if (preg_match($pattern, $ua, $tmp)) {
				$os = str_replace($str_replacer, $tmp, $replace);
				break;
			}
		}
		foreach ($browsers as $pattern => $replace) {
			if (preg_match($pattern, $ua, $tmp)) {
				$browser = str_replace($str_replacer, $tmp, $replace);
				break;
			}
		}

		$additions = array();
		if (preg_match('#mobi#i', $ua))
			$additions[] = 'mobile';
		if (preg_match('#touch#i', $ua))
			$additions[] = 'touch';

		if ($additions)
			$additions = ' (' . implode(', ', $additions) . ')';
		else
			$additions = '';

		return "$os/$browser$additions";
	}

	$email = '326374@danwolff.se';
	$issuer = 'login.persona.org';

	$login = true;

	$files = array(
		'logs/access.log',
	);


	chdir(dirname(__DIR__));
	session_start();
	if (isset($_POST['logout'])) {
		unset($_SESSION['logged_in']);
	} elseif (isset($_POST['assertion'])) {
		$ch = curl_init('https://login.persona.org/verify');
		curl_setopt($ch, CURLOPT_POST, true);
		$protocol = empty($_SERVER['HTTPS']) ? 'http' : 'https';
		$assertion = urlencode($_POST['assertion']);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post="assertion=$assertion&audience=$protocol://$_SERVER[SERVER_NAME]");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($ch);
		#echo $assertion;
		echo $data;
		$data = json_decode($data, true);
		if ($data['status'] == 'okay' && $data['email'] == $email && time() < $data['expires'] && $data['issuer'] == $issuer) {
			$_SESSION['logged_in'] = $data['email'];
			$login = false;
		}
	} elseif (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == $email) {
		$login = false;
	}
	if ($login) :
?>
<meta name="robots" content="noindex,nofollow" />
<form method="post">
	<input type="hidden" name="assertion">
	<button type="button">Login</button>
</form>
<script src="https://login.persona.org/include.js"></script>
<script>
	function $(tag) { return document.getElementsByTagName(tag)[0] }
	$("button").onclick = function () {
		navigator.id.get(function (assertion) {
			if (assertion) {
				$("input").value = assertion;
				$("form").submit();
			}
		}, {
			allowPersistent: true
		});
	};
</script>
<?php
		exit;
	endif;

	if (isset($_GET['json'])) {
		header('Content-TYpe: application/json');

		$columns = array(
			array(
				'id' => 'date',
				'type' => 'x',
			),
			array(
				'id' => 'path',
				'type' => 'filterable',
				'desc' => 'Path',
			),
			array(
				'id' => 'platform',
				'type' => 'filterable',
				'desc' => 'Platform',
			),
			array(
				'id' => 'platform-version',
				'type' => 'filterable',
				'desc' => 'Platform version',
			),
			array(
				'id' => 'browser',
				'type' => 'filterable',
				'desc' => 'Browser',
			),
			array(
				'id' => 'browser-version',
				'type' => 'filterable',
				'desc' => 'Browser version',
			),
			array(
				'id' => 'human-access',
				'type' => 'y',
				'desc' => 'Human access',
			),
			array(
				'id' => 'interested-access',
				'type' => 'y',
				'desc' => 'Interested access',
			),
			array(
				'id' => 'use-access',
				'type' => 'y',
				'desc' => 'Use access',
			),

		);

		echo '{"columns":';
		echo json_encode($columns);
		echo '}';
		exit;
	}

?>

<!DOCTYPE html>
<meta charset="utf-8" />
<meta name="robots" content="noindex,nofollow" />
<title>Logs</title>
<style>
	html { font-size: 80%; }
	table { margin-top:1em; border-collapse:collapse; }
	:-moz-any(td, th):not(:first-child) { border-left:1px solid #ccc; }
	td, th { padding:0 0.5em; white-space: nowrap; }
	caption { font:bold 1em sans-serif; cursor:pointer; }
	caption:hover { background:#9cf; }
	table.summary caption { white-space:nowrap; }
	table.summary:not(.all) :-moz-any(td, th):nth-child(n+5) { display:none; }
	.me { opacity: 0.5; }
</style>
<?php if (!$local) : ?><form method="post"><button name="logout">Log out</button></form><?php endif ?>
<?php

	$week_ago = time() - 7*24*60*60;
	$count = array();

	foreach ($files as $file) {
		$items = explode("\n", file_get_contents($file));
		$f = basename($file, '.log');
		echo <<<TABLE_START
			<table>
				<caption>$f</caption>
TABLE_START;


		$count_total[$file] = 0;
		$count_visible[$file] = 0;
		$count_hidden[$file] = 0;
		$count_unique[$file] = array();
		foreach ($items as $item) {
			if (!$item) {
				continue;
			}

			$class = null;
			if (strpos('?qq', $item) || $item[strlen($item) -1] === '0') {
				$class = ' class="me"';
			}
			$count_total[$file]++;
			$row = explode("\t", $item);
			if (strtotime($row[0]) < $week_ago) {
				continue;
			}

			// IP
			$count_unique[$file][$row[4]] = true;

			if ($class) {
				$count_hidden[$file]++;
			} else {
				$count_visible[$file]++;
			}


			echo "<tr$class>";
			$first_cell = true;
			foreach ($row as $cell) {
				if ($first_cell) {
					$first_cell = false;
					$cell_desc = $cell;
					$cell = substr($cell, 5, -3);
				} elseif (trim($cell, '0..9') === '') {
					$cell_desc = $cell;
					$cell = substr($cell, 0, 1);
				} elseif (strpos($cell, 'Mozilla') !== false) {
					$cell_desc = $cell;
					$cell = parse_ua($cell);
				} elseif (strlen($cell) > 20) {
					$cell_desc = $cell;
					$cell = str_replace(array('http://www.', 'http://'), '', $cell);
					$cell = substr($cell, 0, 20) . '…';
				} else {
					$cell_desc = false;
				}
				
				$cell = htmlspecialchars($cell);
				$cell_desc = $cell_desc ? htmlspecialchars($cell_desc) : false;

				echo $cell_desc ? "<td title=\"$cell_desc\">$cell</td>" : "<td>$cell</td>";
			}
			echo "</tr>\n";
		}
		echo '</table>';
	}

	?><table class="summary"><caption
			onclick="this.parentNode.className = this.parentNode.className=='summary all' ? 'summary' : 'summary all';"
		>During the last 7 days</caption>
		<tr><th>log</th>
			<th>visible</th>
			<th>hidden</th>
			<th>unique</th>
			<th>all time total</th></tr>
	<?php

	foreach ($files as $file) {
		$f = basename($file, '.log');
		$unique = count($count_unique[$file]);

		echo "<tr><td>$f</td>
			<td>{$count_visible[$file]}
			<td>{$count_hidden[$file]}
			<td>$unique
			<td>{$count_total[$file]}</td>
		</tr>";
	}
	echo '</table>';
