<?php

$texts = [
    'suggest.title' => [
        'en' => 'Word Suggestions - Play Charades',
        'sv' => 'Förslag på ord - Spela charader',
    ],
    'suggest.heading' => [
        'en' => 'Word Suggestions?',
        'sv' => 'Förslag på ord?',
    ],
    'suggest.intro' => [
        'en' => "Do you have suggestions for new words? Enter them here, and we'll add them to the generator.",
        'sv' => 'Har du förslag på nya ord? Skicka in dem här, så lägger vi till dem i generatorn.',
    ],
    'link.backToGame' => [
        'en' => 'Back to game',
        'sv' => 'Tillbaka till spelet',
    ],
    'heading.category' => [
        'en' => 'Category',
        'sv' => 'Kategori',
    ],
    'btn.sendSuggestion' => [
        'en' => 'Send suggestion',
        'sv' => 'Skicka förslag',
    ],
    'autonym' => [
        'en' => 'English',
        'sv' => 'Svenska',
    ],
    'msg.noWordSubmitted' => [
        'en' => 'You did not submit any word',
        'sv' => 'Du skickade inte in något ord',
    ],
    'msg.cannotSaveWord' => [
        'en' => 'Cannot save word',
        'sv' => 'Kan inte spara ordet',
    ],
    'msg.thanksForTheWord' => [
        'en' => 'Thanks for the word',
        'sv' => 'Tack för ordet',
    ],
];
