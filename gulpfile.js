/* eslint-disable no-var */

const gulp = require('gulp');
const rollup = require('gulp-rollup');
const newer = require('gulp-newer');
const sourcemaps = require('gulp-sourcemaps');
const watch = require('gulp-watch');
const environments = require('gulp-environments');
const plumber = require('gulp-plumber');
const gutil = require('gulp-util');
const rename = require('gulp-rename');
const filter = require('gulp-filter');
const replace = require('gulp-replace');
const template = require('gulp-template');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');

const babel = require('rollup-plugin-babel');

const rimraf = require('rimraf');

const development = environments.development;
const production = environments.production;

const dest = 'dist';

// Patch gulp.src to always use plumber.
const _gulpSrc = gulp.src;
gulp.src = function () {
    return _gulpSrc.apply(gulp, arguments)
        .pipe(plumber({
            errorHandler: function (err) {
                gutil.log(err.stack);
                this.emit('end');
            },
        }));
};

gulp.task('default', ['build']);

gulp.task('build', ['clean'], function () {
    return gulp
        .start('build-html')
        .start('build-js')
        .start('build-css')
        .start('build-static');
});

gulp.task('clean', function (callback) {
    rimraf(dest, callback);
});

gulp.task('build-js', ['clean-js'], function () {
    gulp.src('js/main.js', { read: false })
        .pipe(rollup({
            sourceMap: development(),
            format: 'iife',
            plugins: [
                // npm({ jsnext: false, main: true }),
                // commonjs(),
                babel(),
            ],
        }))

        .pipe(production(uglify()))

        .pipe(development(sourcemaps.write('.')))

        .pipe(gulp.dest(dest));
});

gulp.task('clean-js', function (callback) {
    rimraf(dest + '/@(main.js|main.js.map)', callback);
});

gulp.task('build-css', ['clean-css'], function () {
    const pImport = require('postcss-import');
    const cssnext = require('postcss-cssnext');
    const cssnano = require('cssnano');

    gulp.src('css/main.css')
        .pipe(postcss(
            development()
                ? [pImport, cssnext]
                : [pImport, cssnext, cssnano]
            ))
        .pipe(gulp.dest(dest));
});

gulp.task('clean-css', function (callback) {
    rimraf(dest + '/main.css', callback);
});

gulp.task('build-html', function (callback) {

    const origBabelEnvExists = 'BABEL_ENV' in process.env;
    const origBabelEnv = process.env.BABEL_ENV;
    process.env.BABEL_ENV = 'node';

    require('babel-register');
    require('babel-polyfill');
    const processPage = require('./build/server-render').processPage;

    if (origBabelEnvExists) {
        process.env.BABEL_ENV = origBabelEnv;
    } else {
        delete process.env.BABEL_ENV;
    }

    gulp.src('html/*')
        .pipe(replace(/[\s\S]*/, processHtml));

    var expectedCount = 0;
    var count = 0;

    function processHtml(html) {
        const pages = processPage(html, production());
        pages.forEach(createFile);
        return '';
    }

    function createFile(file) {
        expectedCount++;
        var html = file.html;
        if (production()) {
            html = html
                // Remove white-space.
                .replace(/\n\s+/g, '\n')

                // Render before loading scripts.
                .replace(/<script /g, '<script defer ');

            // Use minified versions.
            html = [
                'react.js',
                'react-dom.js',
                'react-with-addons.js',
                'redux.js',
                'react-redux.js',
                'regeneratorruntime.js',
            ].reduce((html, filename) => {
                return html.replace(
                    filename,
                    filename.replace('.js', '.min.js')
                );
            }, html);
        }

        require('fs').writeFile(dest + '/' + file.filename, html, inc);
        console.log('Create file', file.filename);
    }

    function inc() {
        count++;
        if (expectedCount === count) {
            setTimeout(returnIfEqual, 200);
        }
    }

    function returnIfEqual() {
        if (expectedCount === count) {
            callback();
            callback = function () {};
        }
    }
});

gulp.task('build-static', function () {
    gulp.src(['static/**/*', 'static/.htaccess'])
        .pipe(newer(dest))
        .pipe(gulp.dest(dest));
});

gulp.task('build-prod', function () {
    environments.current(production);

    gulp.start('build');
});

gulp.task('watch', ['build'], function (callback) {
    watch(['js/**/*.js'], function () {
        gulp.start('build-js');

        writeBuildDate();
    });

    watch(['css/**/*.css'], function () {
        gulp.start('build-css');

        writeBuildDate();
    });

    watch(['html/**/*'], function () {
        gulp.start('build-html');
    });

    watch(['static/**/*'], function () {
        gulp.start('build-static');
    });

    writeBuildDate();
});

function writeBuildDate() {
    require('mkdirp').sync(dest);

    require('fs').writeFileSync(dest + '/build-date.txt', new Date());
}
