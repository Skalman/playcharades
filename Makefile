.PHONY: build build-prod watch lint lint-fix clean

build:
	NODE_ENV=development node ./node_modules/.bin/gulp

build-prod:
	NODE_ENV=production node ./node_modules/.bin/gulp

watch:
	NODE_ENV=development node ./node_modules/.bin/gulp watch

lint:
	node ./node_modules/.bin/stylelint css/*.css
	node ./node_modules/.bin/eslint js/ build/

lint-fix:
	node ./node_modules/.bin/eslint --fix js/

clean:
	rm -rf dist/
