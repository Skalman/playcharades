import {renderToString} from 'react-dom/server';
import fs from 'fs';
import {createStore, createProvider} from '../js/redux/store';
import {setLangs} from '../js/redux/actions';
import {translate, langs} from '../js/data/index';
import {_, basename} from '../js/utils';

const getTemplate = _.memoize(function () {
    return _.template(fs.readFileSync(__dirname + '/template.html', 'utf-8'));
});

function getSettingsValue([key, value]) {
    key = key.trim().toLowerCase();
    value = value.trim();
    if (key === 'langs') {
        value = value.split(/,\s*/);
    }

    return [key, value];
}

export function processPage(src, production) {
    const [rawSettings, rawHtml] = splitTwo(src, '\n---\n');
    const settings = rawSettings
        .split('\n')
        .map(line => splitTwo(line, ':'))
        .reduce((obj, line) => {
            const [key, value] = getSettingsValue(line);
            obj[key] = value;

            return obj;
        }, {});

    const htmlAsTemplate = _.template(rawHtml);

    settings.hreflang = langs
        .map(lang => {
            const url = translate(lang, settings.url, false);
            if (url) {
                return `<link rel="alternate" hreflang="${lang}" href="${url}">`;
            }
        })
        .join('');

    const pages = settings.langs.map(lang => {
        const s = Object.assign({}, settings, {
            translate: translate(lang),
            lang: lang,
            title: translate(lang, settings.title),
        });

        let pageHtml;
        if (s.mode === 'react') {
            s.contents = production ? renderReactHtml(s).html : null;
            pageHtml = htmlAsTemplate(s);
        } else if (s.mode === 'php') {
            pageHtml = htmlAsTemplate(s);
        } else {
            // Use template for layout.
            s.contents = htmlAsTemplate(s);
            pageHtml = getTemplate()(s);
        }

        return {
            filename: getFilename(lang, s.url, s.mode),
            html: pageHtml,
        };
    });

    return pages;
}

function splitTwo(text, delimiter) {
    const index = text.indexOf(delimiter);
    if (index === -1) {
        return [text, null];
    } else {
        return [text.substr(0, index), text.substr(index + delimiter.length)];
    }
}

const renderReactHtmlCached = _.memoize(function () {
    const store = createStore();

    const provider = createProvider(store);

    const pages = [];

    store.dispatch(setLangs(['en'], false));
    pages.push({
        lang: 'en',
        url: 'url.home',
        html: renderToString(provider),
    });


    store.dispatch(setLangs(['sv'], false));
    pages.push({
        lang: 'sv',
        url: 'url.home',
        html: renderToString(provider),
    });

    return pages;
});

function renderReactHtml(opts) {
    const pages = renderReactHtmlCached();

    if (!opts) {
        // Not requesting specific page, so return all.
        return pages;
    } else {
        // Find specific page.
        return pages.find(p =>
            p.lang === opts.lang
            && p.url === opts.url
        );
    }
}

function getFilename(lang, textId, mode) {
    const url = basename(translate(lang, textId));
    const ext = mode === 'php' ? '.php' : '.html';

    if (url === '') {
        return 'index' + ext;
    } else {
        return url.replace(/\.(html|php)$/, '') + ext;
    }
}
