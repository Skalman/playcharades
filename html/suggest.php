Langs: en, sv
Url: url.suggest
Mode: php
---
<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

  require 'translate.php';
  $langs = ['en', 'sv'];
  function trans($id) {
    global $texts;
    $lang = '<%= lang %>';
    return $texts[$id][$lang];
  }

	if (@$_POST['cat'] && preg_match('/^[a-zA-Z]+$/', $_POST['cat'])) {
		$file = "suggestions/$_POST[cat].js";
    $byLang = [];
		foreach ($langs as $lang) {
      $word = @$_POST[$lang];
			if ($word !== '' && $word !== null) {
				$word = str_replace("'", "\\'", $word);
				$byLang[$lang] = "$lang: '$word'";
			}
		}
		$byLang = array_filter($byLang);
		$byLang = implode(', ', $byLang);
		if (!$byLang) {
			$msg = trans('msg.noWordSubmitted');
			$msgType = 'failure';
		} else {
			$byLang = "{{$byLang}},\n";
			$fh = @fopen($file, 'a');
			if (!$fh) {
				$msg = trans('msg.cannotSaveWord');
				$msgType = 'failure';
			} else {
				fwrite($fh, $byLang);
				fclose($fh);
				chmod($file, 0666);

				$msg = trans('msg.thanksForTheWord');
				$msgType = 'success';
			}
		}

	}

?>
<!DOCTYPE html>
<html lang="<%= lang %>">
<head>
	<meta charset="utf-8">
	<title><?= trans('suggest.title') ?></title>
	<link rel="stylesheet" href="main.css">
	<link rel="icon" href="img/stick-figure.ico">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <%= hreflang %>
</head>
<body>
	<div id="app">
		<div class="charades-app">

<h1>
    <a href="<%= translate('url.home') %>">
        <img src="img/stick-figure.png" alt="">
    </a>
    <?= trans('suggest.heading') ?>
</h1>

<p>
	<a href="<%= translate('url.home') %>"><?= trans('link.backToGame') ?></a>
</p>

<p>
  <?= trans('suggest.intro') ?>
</p>

<form method="post">
	<style scoped>
		label {
			display: block;
		}
		.success {
			background: #9f9;
			padding: 1em;
		}
		.failure {
			background: #f99;
			padding: 1em;
		}
	</style>

	<?php
		if (@$msg) {
			?>
			<div class="<?= $msgType ?>"><?= $msg ?></div>
			<?php
		}
	?>

	<p>
		<?= trans('heading.category') ?>
	</p>
	<ul class="list-inline list-toggle">
  <?php
    $cats = <%= JSON.stringify(
      'easy moderate hard veryHard phrases celebrities characters moviesAndTvShows songs books'
        .split(' ')
        .map(c => [c, translate('cat.' + c)])
      ) %>;

			$selCat = @$_POST['cat'] ? $_POST['cat'] : 'easy';

      foreach ($cats as $cat) {
        $id = $cat[0];
				?>
				<li>
					<input type="radio" name="cat" id="<?=$id?>" value="<?=$id?>" <?=$id===$selCat ? 'checked' : ''?>>
					<label for="<?=$id?>"><?=$cat[1]?></label>
				</li>
				<?php

      }
  ?>
	</ul>

  <?php
    $sortedLangs = array_merge(['<%= lang %>'], $langs);
    $sortedLangs = array_unique($sortedLangs);

    foreach ($sortedLangs as $lang) {
      ?>
        <p>
          <label>
            <?= $texts['autonym'][$lang] ?><br>
            <input name="<?= $lang ?>">
          </label>
        </p>
      <?php
    }
  ?>

	<p>
		<button type="submit" class="btn btn-main"><?= trans('btn.sendSuggestion') ?></button>
	</p>
</form>

		</div>
	</div>
</body>
</html>
